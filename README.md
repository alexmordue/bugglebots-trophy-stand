# HomeKit compatible Bugglebots SK6812 RGBW LED lit trophy 

## Introduction

This is a project to create a trophy display for a laser cut acrylic competitor coin from the Bugglebots competition, but could be used for other things too I suppose.

![Stand](/docs/stand.jpg)

[Bugglebots Trophy Video on YouTube](http://www.youtube.com/watch?v=jCvK03UXqlE)

## You will need

Hardware (about $5 of parts, or less in quantity)
* A Wemos D1 Mini or clone (Other ESP boards might work)
* 5 x SK6812 RGBW leds. I bought a 10 x 10 grid of the ones on little round PCBs (the round PCBs are about 10mm in diameter)
* Some small amount of thin insulated wire e.g. old ribbon cable or whatever

Other things
* A computer to flash the code to the ESP8266
* A 3D printer or access to one for about 40 minutes of printing
* Some m2 screws or glue to stick the bottom on if it doesn't just stay on it's own
* A little bit of electrical tape

## 3D printed case

STLs and Fusion 360 files are included [3d/](3d/) so you can play with them and print them.

How it all goes together should be pretty self explanatory, let me know if not and I'll update this.

## Wiring

Wire the 5 x SK6812 together in a chain, with the boards edge to edge (5V to 5V, GND to GND, DOUTs to DINs). With a short length of wire (40mm or so), solder the 5V line on the DIN end of your chain to 5V on the Wemos board, the SK6812 GND to the GND on the Wemos board and the DIN to the RX on the Wemos board. That's it.

## Firmware

Most of the code comes from [esp-homekit-demo](https://github.com/maximkulkin/esp-homekit-demo) with some minor tweaks and mashing together of different examples and adapting it to run slightly different LED strips. If you want to build the firmware for this for yourself see https://github.com/maximkulkin/esp-homekit-demo for more details than below.

### Flashing it to an ESP8266

There is a pre-built version in [firmware/firmware/](firmware/firmware/) , flash it using esptool.py or similar, I found I had to use the `DOUT` flash mode on my Wemos D1 mini boards.

#### Linux
```
# On linux
cd firmware/firmware
esptool.py -p /dev/ttyUSB0 --baud 115200 write_flash -fs 32m -fm dout -ff 40m 0x0 rboot.bin 0x1000 blank_config.bin 0x2000 buggletrophy.bin 
```

#### Windows

You can use esptool.py as above for linux, or use the NodeMCU flasher GUI tool https://github.com/nodemcu/nodemcu-flasher/raw/master/Win64/Release/ESP8266Flasher.exe

Configure it as follows (your paths will be different for the 3 .bin files):

![Advanced](/docs/nodemcu-flasher-advanced.png)

![Config](/docs/nodemcu-flasher-config.png)

![Operation](/docs/nodemcu-flasher-operation.png)

## Using it

* Supply power to the Wemos board (micro USB).
* Look in your phones wifi settings for the access point "Bugglebots Trophy-xxxxxx"
* Connect to it and you should shortly be presented with a web page so that you can connect the trophy to your homes wifi network
* Once you hit connect, the trophy stand will try and connect to your homes wifi and you should be automatically disconnected from the "Bugglebots Trophy-xxxxxx" wifi
* Assuming it connected okay you can now add the trophy as an accessory in the Apple Home app (the pairing code is 111-11-111)
* Play

## Building Firmware from scratch

There is a pre-built version in [firmware/firmware/](firmware/firmware/) so you don't have to build it, so skip this part unless you are interested, but if you want to you can by:

* Follow the instructions in https://github.com/maximkulkin/esp-homekit-demo#usage from step 3 onwards to install `esp-open-sdk` and `esp-open-rtos` 
* Make sure to set up ENV variables the 3 most important are `PATH`, `SDK_PATH` and `FLASH_MODE` (assuming you are using an esp8266 with 4MB (Like most Wemos D1 minis), everything else is fine and doesn't need setting.
* Get this repo and all it's submodules `git clone --recursive https://gitlab.com/alexmordue/bugglebots-trophy-stand.git`
* Build the firmware `cd bugglebots-trophy-stand; make -C firmware`

